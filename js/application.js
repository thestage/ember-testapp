window.TheStage = Ember.Application.create({
  LOG_TRANSITIONS: true
});

TheStage.ApplicationAdapter = DS.LSAdapter.extend({
  namespace: 'TheStage-emberjs'
});

// ROUTER

TheStage.Router.map(function() {
  this.resource("items", function(){
    this.route("show", { path: ":item_id" });
  });
});

TheStage.ApplicationRoute = Ember.Route.extend({
  beforeModel: function() {
    this.transitionTo('items.index');
  }
});

TheStage.ItemsIndexRoute = Ember.Route.extend({
  model: function(){
    return this.store.find('item');
  },
  setupController: function(controller,model){
    // url parsing for filters
    if ( /id=(.[^&]*)/.test(location.hash) ) { controller.set('idFilter',/id=(.[^&]*)/.exec(location.hash)[1]) };
    if ( /code=(.[^&]*)/.test(location.hash) ) { controller.set('codeFilter',/code=(.[^&]*)/.exec(location.hash)[1]) };
    this._super(controller,model);
  }
});

TheStage.ItemsShowRoute = Ember.Route.extend({
  model: function(params){
    return this.store.find('item',params.item_id);
  },
  setupController: function(controller,model){
    this._super(controller,model);
    var list = this.controllerFor('items.index').get('filteredItems');
    list.then(function(){
        var index = list.indexOf(model);
        controller.set('prev',list.objectAt(index - 1));
        controller.set('next',list.objectAt(index + 1));
    });
  }
});

// MODEL

TheStage.Item = DS.Model.extend({
  code: DS.attr()
})

// CONTROLLERS

TheStage.ItemsController = Ember.ArrayController.extend({
  itemsCount: 0
});

TheStage.ItemsIndexController = Ember.ArrayController.extend({
  needs: "items",
  filteredItems: function() {
    var code_filter = this.get('codeFilter')||'';
    var id_filter = this.get('idFilter')||'';
    // url
    var search_query = '?';
    if (code_filter) {search_query += 'code='+code_filter+'&'};
    if (id_filter) {search_query += 'id='+id_filter};
    search_query = search_query.replace(/[&\?]$/,'');
    var new_url = location.hash.replace(/\?.*/,'')+search_query;
    history.pushState({},null,new_url);
    // filtering 
    var list = this.store.filter('item',{},function(item){
      var id_patt = new RegExp(id_filter);
      var code_patt = new RegExp(code_filter);
      return id_patt && id_patt.test(item.get('id')) && code_patt && code_patt.test(item.get('code'));
    });
    var itemsController = this.get('controllers.items');
    list.then(function(){
      itemsController.set('itemsCount',list.get('length'));
    });
    return list;
  }.property('model.@each.code','idFilter','codeFilter'),
  actions: {
   add: function() {
      var item = this.store.createRecord('item', {code: Math.random()});
      item.save();
    }
  }
});

TheStage.ItemController = Ember.ObjectController.extend({
  actions: {
    delete: function(item) {
      if (confirm('Remove item ' + item.id + '?')) {
        item.deleteRecord();
        item.save();
      }
    }
  }
});
